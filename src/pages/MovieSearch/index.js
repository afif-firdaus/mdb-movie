import React, { useState, useEffect, Fragment } from 'react';
import axios from 'axios';
import { useParams, useHistory } from 'react-router-dom';
import './MovieSearch.scss';
import { MovieDetailLoader, Header, Footer } from '../../components';
import { connect, useDispatch } from 'react-redux';

const MovieSearch = (props) => {
  let { query } = useParams();
  const dispatch = useDispatch()
  // let query = localStorage.getItem("query");
  // console.log('query', query)
  // localStorage.setItem("idmovie", id);
  // let idmovie = localStorage.getItem('')

  const [movieSearch, setMovieSearch] = useState([]);
  // const [trailer, setTrailer] = useState();
  // const [isOpen, setOpen] = useState(false)


  useEffect(() => {
    const timer = setTimeout(() => {
      getMovieSearch();
      // dispatch({type: 'CHANGE_ISLOADING', value: false})
    }, 2500);
    return () => clearTimeout(timer)
  });

  const getMovieSearch = () => {
    axios.get(`https://api.themoviedb.org/3/search/movie?api_key=cdcf3f48ca32835aac91a6ddc8eacfec&query=${query}&page=1`)
    .then((res) => {
      // console.log("Search", res.data.results)
      setMovieSearch(res.data.results)
      dispatch({type: 'CHANGE_ISLOADING', value: false})
    })
  }

  const history = useHistory();

  return (
    <div className="movie-search-main">
    <Header />
    <div className="movie-search">
      {
        props.isLoading === true ? <MovieDetailLoader/> : (
          <Fragment>
            <h3>Search result for '{query}'</h3>
            <div className="movie-search-section">
            {
              movieSearch !== undefined && movieSearch.length > 0 ? movieSearch.map((movie, index) => {
                return (
                  <div key={index} className="movie-card-search" onClick={() => history.push(`/movie-details/${movie.id}`)}>
                    <img src={`https://image.tmdb.org/t/p/original${movie.poster_path}`} alt="" />
                    <p>{movie.title}</p>
                  </div>
                )
              }) : <div className="search-notfound">
                <h4>Sorry, there are no result for '{query}'</h4>
              </div>
            }
            </div>
          </Fragment>
        )
      }
    </div>
    <Footer/>
    </div>
  )
}

const reduxState = (state) => ({
  isLoading: state.isLoading
})

export default connect(reduxState)(MovieSearch);