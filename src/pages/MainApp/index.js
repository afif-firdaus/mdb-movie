import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Homepage from '../Homepage';
import MovieDetails from '../MovieDetails';
import MovieSearch from '../MovieSearch';
import './MainApp.scss';

const MainApp = () => {
  return (
    <Router>
      <div className="main-app">
        <div className="main-content">
            <Switch>
              {/* <Route exact path="/movie" component={MovieSearch} /> */}
              <Route  path="/movie-search/:query" component={MovieSearch}/>
              <Route  path="/movie-details/:id" component={MovieDetails}/>
              <Route  path="/" component={Homepage}/>
            </Switch>
        </div>
      </div>
    </Router>
  )
}

export default MainApp;
