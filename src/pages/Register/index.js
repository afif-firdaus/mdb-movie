import React, { Component } from 'react'
import { Input, Button, Gap } from '../../components';
import './Register.scss';
import { connect } from 'react-redux';
import {registerUserAPI} from '../../config/redux/action';
import { Link } from 'react-router-dom';

class Register extends Component {
  state = {
    email: '',
    password: '',
    displayName: '',
    error: ''
  }

  handleInputChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleRegisterSubmit = async () => {
    const { displayName, email, password } = this.state;
    // console.log("STATE", this.state)
    const { history } = this.props;
    const res = await this.props.registerAPI({ displayName, email, password })
    .catch(err => {
      // console.log(err, "ERRROOORRRR")
      this.setState({
        error: err.message
      })
    })
    if (res) {
      this.setState({
        email: '',
        password: '',
        displayName: '',
        error: ''
      })
      history.push('/login')
    } else {
      // console.log('Register Failed')
    }
  }

  render(){
    // console.log(this.props, "kakdskakd")
    return (
      <div className="register-page">
        <div className="register-card">
          <h1>Create Account</h1>
          <Gap height={20}/>
          <Input label="Your name" id="displayName" type="text" onChange={this.handleInputChange} value={this.state.displayName}/>
          <Gap height={10}/>
          <Input label="Email" id="email" type="email" onChange={this.handleInputChange} value={this.state.email}/>
          <Gap height={10}/>
          <Input label="Password" placeholder="at least 6 character" id="password" type="password" onChange={this.handleInputChange} value={this.state.password}/>
          <p className="error-msg">{this.state.error}</p>
          <Gap height={15}/>
          {
            this.props.isLoading === true ? <button className="btns-anima">Loading...</button> : 
            <Button title="Create account" onClick={this.handleRegisterSubmit} />
          }
          <Gap height={10}/>
          <div className="login">
            <p>Already have an account?</p>
            <Gap width={5}/>
            <Link className="link-item" to="/login">Sign-In</Link>
          </div>
        </div>
      </div>
    )
  }
}

const reduxState = (state) => ({
  isLoading: state.isLoading
});

const reduxDispatch = (dispatch) => ({
  registerAPI: (data) => dispatch(registerUserAPI(data))
})

export default connect(reduxState, reduxDispatch)(Register);