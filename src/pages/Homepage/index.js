import React from 'react';
import { Gap, Movie } from '../../components';
import './Homepage.scss';
import { CarouselComp, Header, Footer } from '../../components';

const Homepage = () => {
  return (
    <div className="homepage-main">
      <Header />
      <CarouselComp />
      <Gap height={40} />
      <div className="homepage">
        <Movie />
      </div>
      <Gap height={20} />
      <Footer />
    </div>
  )
}

export default Homepage;