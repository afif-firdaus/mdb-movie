import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import './MovieDetails.scss';
import star from '../../assets/star.svg';
import ModalVideo from 'react-modal-video';
import { MovieDetailLoader, Header, Footer } from '../../components';

const MovieDetails = () => {
  let { id } = useParams();
  const history = useHistory();
  // localStorage.setItem("idmovie", id);
  // let idmovie = localStorage.getItem('idmovie')

  const [movieData, setMovieData] = useState([]);
  const [movieCast, setMovieCast] = useState([]);
  const [trailer, setTrailer] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [isOpen, setOpen] = useState(false)


  useEffect(() => {
    setTimeout(() => {
      axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=cdcf3f48ca32835aac91a6ddc8eacfec`)
      .then((res) => {
        setMovieData(res.data)
        setIsLoading(false);
      })
    }, 2000)
  }, []);

  useEffect(() => {
    axios.get(`https://api.themoviedb.org/3/movie/${id}/credits?api_key=cdcf3f48ca32835aac91a6ddc8eacfec`)
    .then((res) => {
      // console.log("ERSSSS", res)
      setMovieCast(res.data.cast)
    })
  }, []);
  
  const hadleWatch = () => {
    axios.get(`https://api.themoviedb.org/3/movie/${id}/videos?api_key=cdcf3f48ca32835aac91a6ddc8eacfec`)
    .then((res) => {
      let trailer = res.data.results
      if (trailer.length >= 0) {
        setTrailer(res.data.results[0].key)
        // console.log('Trailer',res.data.results[0].key)
      }     
      setOpen(true)
    })
  }

  // console.log("EWSSS", movieData.backdrop_path)
  return (
    <div className="movie-details">
      <Header />
        {
          isLoading ? <MovieDetailLoader /> : (
            <div className="main-detail">
              <div className="movie-detail-top">
                <div className="movie-image">
                  <img className="poster" src={`https://image.tmdb.org/t/p/original${movieData.poster_path}`} alt=""/>
                  <img className="background" src={`https://image.tmdb.org/t/p/original${movieData.backdrop_path}`} alt=""/>
                </div>
                <div className="movie-overview">
                  <h1>{movieData.title}</h1>
                  <div className="rating">
                    <img src={star} alt=""/>
                    <div className="score">
                      <p><span>{movieData.vote_average}</span>/10</p>
                      <p className="total-score">{movieData.vote_count} vote</p>
                    </div>
                  </div>
                  <h3>Release Date:</h3>
                  <p>{movieData.release_date}</p>
                  <h3>Overview:</h3>
                  <p>{movieData.overview}</p>
                  {/* <p>Genre: {movieData.genres[0].name}, {movieData.genres[1].name}</p> */}
                  <ModalVideo channel='youtube' autoplay isOpen={isOpen} videoId={trailer} onClose={() => setOpen(false)} />
                  <div className="btn-group">
                    <button className="btn-primary trailer" onClick={hadleWatch}>Watch Trailer</button>
                    <button className="btn-primary reviews" onClick={() => history.push(`/movie-review/${id}`)}>See User Reviews</button>
                  </div>
                </div>
              </div>
              <h2 className="header-cast">CAST</h2>
              <div className="movie-detail-bottom">
                {
                  movieCast.map((cast) => {
                    return (
                      <div className="cast-card">
                        <img src={`https://image.tmdb.org/t/p/original${cast.profile_path}`} alt="" />
                        <h5>{cast.character}</h5>
                        <p>{cast.name}</p>
                      </div>
                    )
                  })
                }
              </div>
            </div>
          )
        }
        <Footer/>
      </div>
  )
}

export default MovieDetails;