import React, {Component} from 'react'
import { Button, Gap, Input } from '../../components';
import './Login.scss';
import { connect } from 'react-redux';
import { loginUserAPI } from '../../config/redux/action';
import { Link } from 'react-router-dom';

  class Login extends Component {
    state = {
      email: '',
      password: '',
      displayName: '',
      error: ''
    }
  
    handleChangeInput = (e) => {
      this.setState({
        [e.target.id]: e.target.value
      })
    }
  
    handleLoginSubmit = async () => {
      const { email, password } = this.state;
      const { history } = this.props;
      const res = await this.props.loginAPI({ email, password })
      .catch(err => {
        this.setState({
          error: err.message
        })
      })
      if (res) {
        // console.log('ini res', res)
        localStorage.setItem('userData', JSON.stringify(res))
        this.setState({
          email: '',
          password: '',
          error: ''
        })
        // console.log('Login Success')

        history.push('/')
      } else {
        // console.log('Login Failed')
      }
    }
  
  
    render(){
      // console.log(this.state)
      return (
        <div className="login-page">
          <div className="login-card">
            <h1>Sign-In</h1>
            <Gap height={20}/>
            <Input label="Email" id="email" type="email" onChange={this.handleChangeInput} value={this.state.email}/>
            <Gap height={10}/>
            <Input label="Password" id="password" type="password" onChange={this.handleChangeInput} value={this.state.password}/>
            <p className="error-msgs">{this.state.error}</p>
            <Gap height={15}/>
            {
              this.props.isLoading === true ? <button className="btns-anima">Loading...</button> : 
              <Button title="Sign-In" onClick={this.handleLoginSubmit}/>
            }
            <Gap height={30}/>
            <div className="hr">
              <hr/>
              <p>New to MDb?</p>
            </div>
            <Gap height={10}/>
            <Link to="/register">
            <Button className="btn-gray" title="Create MDb account"/>
            </Link>
            <Gap height={5}/>
          </div>
        </div>
      )
    }
  }

  const reduxState = (state) => ({
    isLoading: state.isLoading
  });
  
  const reduxDispatch = (dispatch) => ({
    loginAPI: (data) => dispatch(loginUserAPI(data))
  });


export default connect(reduxState, reduxDispatch)(Login);