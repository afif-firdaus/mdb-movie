import Homepage from './Homepage';
import Login from './Login';
import Register from './Register';
import MainApp from './MainApp';
import MovieReview from './MovieReview';
import MovieSearch from './MovieSearch'
import MovieDetails from './MovieDetails'


export { Homepage, Login, Register, MainApp, MovieDetails, MovieSearch, MovieReview }