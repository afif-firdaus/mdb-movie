import React, { Fragment, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { MovieDetailLoader, Header, Footer, NotLogin } from '../../components';
import './MovieReview.scss';
import axios from 'axios';
import ReactStars from "react-rating-stars-component";
import user from '../../assets/user.png';
import moment from 'moment';
import {connect} from 'react-redux';

const MovieReview = (props) => {
  let { id } = useParams();

  const [movieReview, setMovieReview] = useState([]);
  const [movieImage, setMovieImage] = useState([]);
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    getMovieReview();
  }, [])

  const getMovieReview = () => {
    setTimeout(() => {
      axios.get(`https://api.themoviedb.org/3/movie/${id}/reviews?api_key=cdcf3f48ca32835aac91a6ddc8eacfec&page=1`)
      .then((res) => {
        // console.log("RESPONID", res)
        setMovieReview(res.data.results)
        setIsLoading(false);
      })
    }, 2000)
  }

  useEffect(() => {
    axios.get(`https://api.themoviedb.org/3/movie/${id}/images?api_key=cdcf3f48ca32835aac91a6ddc8eacfec`)
      .then((res) => {
        // console.log("RES IMAGE", res)
        let image = res.data.backdrops
        if (image.length >= 0) {
        setMovieImage(res.data.backdrops[0].file_path)
        // console.log('image',res.data.backdrops[0].file_path)
      }     
    })
  }, [])


  return (
    <div className="movie-review">
      <Header/>
      {
        isLoading ? <MovieDetailLoader/> : (
          <Fragment>
            {
              props.isLogin === false ? <NotLogin/> : (
                <Fragment>
                <img className="movie-backdrop" src={`https://image.tmdb.org/t/p/original${movieImage}`} alt=""/>
                <div className="review-section">
                {
                  movieReview.length > 0 ? movieReview.map((review, index) => {
                    return (
                      <div className="review-card">
                        <div className="review-image">
                          <img src={review.author_details.avatar_path !== null ? `https://image.tmdb.org/t/p/original${review.author_details.avatar_path}` : user} alt="" />
                        </div>
                        <div className="review-text">
                          <h3>{review.author}</h3>
                          <ReactStars
                            count={5}
                            size={24}
                            value={review.author_details.rating/2}
                            isHalf={true}
                            edit={false}
                            />
                          <p>{moment(review.created_at).format('lll')}</p>
                          <p className="content-review">{review.content}</p>
                        </div>
                      </div>
                    )
                  }) : 
                  <div className="review-card">
                    <div className="review-text">
                      <h3 className="no-review">There are no reviews yet</h3>
                    </div>
                  </div>
                }
              </div>
              </Fragment>
              )
            }
        </Fragment>
        )
      }
      <Footer/>
    </div>
  )
};

const reduxState = (state) => ({
  isLogin: state.isLogin
})

export default connect(reduxState)(MovieReview)