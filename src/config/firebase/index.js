import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

var firebaseConfig = {
  apiKey: "AIzaSyD4Tic4SxOTBxsORH7uO4ojW_iOCDir8BY",
  authDomain: "movie-app-e7a3e.firebaseapp.com",
  databaseURL: "movie-app-e7a3e-default-rtdb.firebaseio.com",
  projectId: "movie-app-e7a3e",
  storageBucket: "movie-app-e7a3e.appspot.com",
  messagingSenderId: "961685700941",
  appId: "1:961685700941:web:fe8be7c475fef1d1a47704"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// export const database = firebase.database();

export default firebase;