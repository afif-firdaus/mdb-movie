import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Login, Register, Homepage } from '../../pages';
import { MovieDetails, MovieReview, MovieSearch } from '../../pages';

const Routes = () => {
  return (
    <Router>
      <div className="App">
      <Switch>
          <Route path="/register" component={Register} />
          <Route path="/login" component={Login}/>
          <Route path="/movie-search/:query" component={MovieSearch}/>
          <Route path="/movie-details/:id" component={MovieDetails}/>
          <Route path="/movie-review/:id" component={MovieReview}/>
          <Route exact  path="/" component={Homepage}/>
        </Switch>
        </div>
    </Router>
  )
}

export default Routes;