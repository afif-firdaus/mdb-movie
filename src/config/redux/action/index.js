import firebase from '../../firebase';

export const registerUserAPI = (data) => (dispatch) => {
  return new Promise((resolve, reject) => {
    dispatch({type: 'CHANGE_ISLOADING', value: true});
    firebase.auth().createUserWithEmailAndPassword(data.email, data.password)
    .then((res) => {
      // console.log("RESPONE", res)
      const user = firebase.auth().currentUser;
        user.updateProfile({
          displayName: data.displayName
        })
      dispatch({type: 'CHANGE_ISLOADING', value: false})
      resolve(true)
      // console.log(user, 'USER')
    })
    .catch((err) => {
      dispatch({type: 'CHANGE_ISLOADING', value: false})
      reject(err);
    })
  })
}

export const loginUserAPI = (data) => (dispatch) => {
  return new Promise((resolve, reject) => {
    dispatch({type: 'CHANGE_ISLOADING', value: true});
    firebase.auth().signInWithEmailAndPassword(data.email, data.password)
    .then((res) => {
      // console.log("login succes", res);
      const user = {
        email: res.user.email,
        uid: res.user.uid,
        emailVerified: res.user.emailVerified,
        refreshToken: res.user.refreshToken,
        displayName: res.user.displayName
      }
      dispatch({type: 'CHANGE_ISLOADING', value: false});
      dispatch({type: 'CHANGE_ISLOGIN', value: true});
      dispatch({type: 'CHANGE_USER', value: user});
      resolve(user)
    })
    .catch((error) => {
      // console.log(errorCode, errorMessage)
      dispatch({type: 'CHANGE_ISLOADING', value: false});
      dispatch({type: 'CHANGE_ISLOGIN', value: false});
      reject(error)
    })
  })
}