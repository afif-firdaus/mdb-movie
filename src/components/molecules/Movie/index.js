import React, { Fragment, useEffect, useState } from 'react';
import './Movie.scss';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import MovieDetailLoader from '../MovieDetailLoader';
import ReactPaginate from 'react-paginate';
import './Pagination.css'


const Movie = () => {
  const [movieData, setMovieData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [category, setCategory] = useState('')
  const [activePage, setActivePage] = useState(5);
  const [totalPage, setTotalPage] = useState();

  const handlePageChange = (e) => {
    let selected = e.selected + 1;
    setActivePage(selected)
  }

  useEffect(() => {
    getMovie();
  }, [activePage]);

  function getMovie() {
    setIsLoading(false)
      axios.get(`https://api.themoviedb.org/3/movie/popular?api_key=cdcf3f48ca32835aac91a6ddc8eacfec&page=${activePage}`)
      .then((res) => {
        // console.log("MOVIE", res.data.total_pages)
        let data = res.data.results
        setTotalPage(res.data.total_pages)
        setMovieData(data)
        setCategory('Popular')
      })
  }

  const handlePopular = (e) => {
    e.preventDefault();
    setIsLoading(true);
    setTimeout(() => {
      axios.get(`https://api.themoviedb.org/3/movie/popular?api_key=cdcf3f48ca32835aac91a6ddc8eacfec&page=${activePage}`)
      .then((res) => {
        setTotalPage(res.data.total_pages)
        setMovieData(res.data.results)
        setCategory('Popular')
        setIsLoading(false)
      })
    }, 2000)
  }

  const handleTopRated = (e) => {
    e.preventDefault();
    setIsLoading(true);
    setTimeout(() => {
      axios.get(`https://api.themoviedb.org/3/movie/top_rated?api_key=cdcf3f48ca32835aac91a6ddc8eacfec&page=${activePage}`)
      .then((res) => {
        setTotalPage(res.data.total_pages)
        setMovieData(res.data.results)
        setCategory('Top Rated')
        setIsLoading(false)
      })
    }, 2000)
  }

  const handleUpcoming = (e) => {
    e.preventDefault();
    setIsLoading(true);
    setTimeout(() => {
      axios.get(`https://api.themoviedb.org/3/movie/upcoming?api_key=cdcf3f48ca32835aac91a6ddc8eacfec&page=${activePage}`)
      .then((res) => {
        setTotalPage(res.data.total_pages)
        setMovieData(res.data.results)
        setCategory('Upcoming')
        setIsLoading(false)
      })
    }, 2000)
  }

  const history = useHistory();
  // console.log("MOVIE", movieData)
  return (
    <div className="movies">
      <div className="btn-category">
        <button onClick={handlePopular}>Popular</button>
        <button onClick={handleTopRated}>Top Rated</button>
        <button onClick={handleUpcoming}>Upcoming</button>
      </div>
      {
        isLoading ? <MovieDetailLoader /> : 
      <Fragment>
      <h3>{category === 'Popular' ? "What's Popular" : category === 'Top Rated' ? 'Top Rated Movie' : 'Upcoming Movie'}</h3>
      <div className="movie">
      {
        movieData.map((movie, index) => {
          return (
            <div key={index} className="movie-card" onClick={() => history.push(`/movie-details/${movie.id}`)}>
              <img src={`https://image.tmdb.org/t/p/original${movie.poster_path}`} alt="" />
              <p>{movie.title}</p>
            </div>
          )
        })
      }
      </div>
      <ReactPaginate
          previousLabel={'previous'}
          nextLabel={'next'}
          breakLabel={'...'}
          breakClassName={'break-me'}
          pageCount={totalPage}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={handlePageChange}
          containerClassName={'pagination'}
          subContainerClassName={'pages pagination'}
          activeClassName={'active'}
        />
      </Fragment>
      }
    </div>
  )
}

export default Movie;