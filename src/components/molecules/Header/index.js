import React, { useState } from 'react';
import { Button, SearchBar } from '../../atoms';
import './Header.scss';
import {connect} from 'react-redux';
import { useHistory, Link } from 'react-router-dom';
import user from '../../../assets/user.svg';
import { useDispatch } from 'react-redux';

const Header = (props) => {
  const dispatch = useDispatch()
  // console.log('props',props)
  const [sortOpen, setSortOpen] = useState(false);
  // console.log('state',state)
  const history = useHistory();
  
  const handleInputChange = (e) => {
    e.target.id = e.target.value
    // console.log(e.target.id)
  }

  const handleKeyPress = (e) => {
      if (e.key === 'Enter') {
          dispatch({type: 'CHANGE_ISLOADING', value: true})
          localStorage.setItem("query", e.target.id);
          let query = localStorage.getItem("query");
          // console.log(query, "querys")
          history.push(`/movie-search/${query}`);
          // window.location.reload();
      }
  }

  const SignOutHandler = () => {
    dispatch({type: 'CHANGE_ISLOGIN', value: false})
    localStorage.clear();
    history.push('/login');
    // console.log("LOGIRUT", logout)
  };

  const userData = JSON.parse(localStorage.getItem('userData'))
  if (userData !== null) {
    dispatch({type: 'CHANGE_ISLOGIN', value: true})
  }
  // console.log(userData, "userdata")

  return (
    <div className="header-com">
      {
        props.isLogin === false ? (
          <div className="header">
            <Link to="/" className="link-to">MDb</Link>
            <SearchBar placeholder="Search movie..." id="search" onChange={handleInputChange} onKeyPress={handleKeyPress} />
            <Button className="header-btn" title="Sign in" onClick={() => history.push('/login')} />
          </div>
        ) : (
          <div className="header">
            <Link to="/" className="link-to">MDb</Link>
            <SearchBar placeholder="Search movie..." id="search" onChange={handleInputChange} onKeyPress={handleKeyPress} />
            <img src={user} alt="" width={40} onClick={() => setSortOpen(!sortOpen)}/>
            {
              sortOpen ? (
                <div className="profile">
                  <p><strong>Hi, {props.user.displayName}</strong></p>
                  <hr/>
                  <p className="sign-out" onClick={SignOutHandler}>Sign out</p>
                </div>
              ) : null
            }
          </div>
        )
      }
    </div>
  )
}

const reduxState = (state) => ({
  isLogin: state.isLogin,
  user: state.user,
  isLoading: state.isLoading
})

// const reduxDispatch = (dispatch) => ({
//   logout: () => dispatch({type: 'CHANGE_ISLOGIN', value: false})
// })

export default connect(reduxState)(Header);