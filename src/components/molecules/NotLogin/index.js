import React from 'react';
import './NotLogin.scss';
import padlock from '../../../assets/padlock.svg'

const NotLogin = () => {
  return (
    <div className="not-login">
      <img src={padlock} alt="" width={70}/>
      <h2>Sign in to see reviews</h2>
    </div>
  )
}

export default NotLogin;