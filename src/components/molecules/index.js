import Header from './Header';
import Footer from './Footer';
import CarouselComp from './Carousel';
import Movie from './Movie';
import Loading from './Loading';
import MovieDetailLoader from './MovieDetailLoader';
import NotLogin from './NotLogin'

export { Header, Footer, CarouselComp, Movie, Loading, MovieDetailLoader, NotLogin };