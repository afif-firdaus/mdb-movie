import React from 'react';
import './Loading.scss'

const Loading = () => {
  return (
    <div className="loader-section">
      <div className="loader"></div>
    </div>
  )
}

export default Loading
