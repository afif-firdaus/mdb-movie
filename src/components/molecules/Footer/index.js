import React from 'react';
import './Footer.scss';
import tmbd from '../../../assets/tmbd.svg';
import react from '../../../assets/react2.svg';
import redux from '../../../assets/redux.svg';
import firebase from '../../../assets/firebase.svg';
import axios from '../../../assets/axios.svg';
import scss from '../../../assets/scss.svg';

const Footer = () => {
  return (
    <div className="footer-com">
      <div className="footer">
        <p className="logo">MDb</p>
        <div className="api">
          <div className="image-1">
            <p>API Source:</p>  
            <img src={tmbd} alt="The Movie DB" width={80}/>
          </div>
          <div>
            <p>Built with:</p>
            <div className="image-2">
              <img src={react} alt="" width={60}/>
              <img src={redux} alt="" width={50}/>
              <img src={firebase} alt="" width={30}/>
              <img src={axios} alt="" width={60}/>
              <img src={scss} alt="" width={50}/>
            </div>
          </div>
        </div>
      </div>
      <div className="copyright">
        <p>Copyright &copy; 2021 &minus; <span>Afif Firdaus</span></p>
      </div>
    </div>
  )
}

export default Footer;