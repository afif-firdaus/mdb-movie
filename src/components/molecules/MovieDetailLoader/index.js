import React from 'react';
import './MovieDetailLoader.css';

const MovieDetailLoader = () => {
  return (
    <div className="movie-detail-loader">
      <div className="loaders"></div>
    </div>
  )
}

export default MovieDetailLoader
