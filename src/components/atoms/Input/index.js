import React from 'react';
import './Input.scss';

const Input = ({label, ...rest}) => {
  return (
    <div className="input-com">
      <label>{label}</label>
      <input {...rest}/>
    </div>
  )
}

export default Input;