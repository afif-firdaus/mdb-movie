import React from 'react';
import './SearchBar.scss';

const SearchBar = ({ ...rest }) => {
  return (
    <div className="search-bar">
      <input {...rest} />
    </div>
  )
}

export default SearchBar;