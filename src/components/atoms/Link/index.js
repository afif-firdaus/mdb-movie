import React from 'react';
import './Link.scss';

const LinkTo = ({ title, ...rest}) => {
  return <p className="link" {...rest}>{title}</p>
}

export default LinkTo;